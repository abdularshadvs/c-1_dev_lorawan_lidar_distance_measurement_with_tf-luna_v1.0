/**
  ******************************************************************************
  * @file    distance.h
  * @author  CDOH Team
  * @brief   This file contains all the functions prototypes for the TF-Luna driver.
  ******************************************************************************
  */

#ifndef APPLICATION_LEVEL_DISTANCE_H_
#define APPLICATION_LEVEL_DISTANCE_H_

#include <stdint.h>

/**
 * @defgroup Battery, TF-Luna
 */

#define BATT_POWER    				1
#define TF_LUNA_POWER				2

#define R1 	10		/*10 K resistor R1 in voltage divider*/
#define R2 	10		/*10 K resistor R2 in voltage divider*/

#define BATT_ENABLE_PORT			GPIOB
#define BATT_ENABLE_PIN				GPIO_PIN_2
#define BATTERY_CHANNEL				ADC_CHANNEL_4

#define TF_Luna_ENABLE_PORT			GPIOB
#define TF_Luna_ENABLE_PIN			GPIO_PIN_14

/**
 * Structure to store sensor data
 */

typedef struct LIDAR
{
	uint16_t  	Signal_Quality ;   // signal quality in arbitrary units
	int16_t  	Temperature ;   // temperature in 0.01 degree Celsius
	uint16_t  	Distance ;   // distance in centimeters
	uint8_t 	sensor_status; //status of the sensor
	uint16_t	battery_level; //battery voltage level

}lidarData_t;

/** @defgroup Distance measurement Exported_Functions, Peripheral Control functions
  * @brief    Peripheral Control functions
  */
uint16_t readBatteryLevel();

void enable(uint8_t pin);
void disable(uint8_t pin);

void distance_measurement_init();
void distance_read(lidarData_t *sensor_data);

#endif /* APPLICATION_LEVEL_DISTANCE_H_ */
