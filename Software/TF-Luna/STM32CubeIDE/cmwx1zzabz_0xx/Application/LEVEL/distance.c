/**
  ******************************************************************************
  * @file	distance.c
  * @author CDOH Team
  * @brief  Driver for TF-Luna
  *         This file provides firmware functions to manage the following
  *         functionalities
  *           + Initializes necessary pins for the sensors
  *           + Read data from the sensors
  * 
  *@verbatim
  ==============================================================================
                        ##### How to use this driver #####
  ==============================================================================
 	 	 	(+) distance_measurement_init() to Initialize TF-Luna sensor for measuring distance
 	 	 	(+) distance_read() to Read the data from TF-Luna 
 	 	 	(+) readBatteryLevel() to Read Battery status
 **/

/**
 * private Includes
 */

#include "distance.h"
#include "b_l072z_lrwan1_bus.h"
#include "TF_Luna.h"
#include "sys_app.h"
#include "adc_if.h"

uint8_t TF_Luna_Default_I2C_Address = TFL_DEF_ADR ;
uint16_t TF_Luna_Default_Frame_Rate = TFL_DEF_FPS ;

TF_Luna_Lidar TF_Luna;

/**
  * @brief  read external battery voltage connected to analog channel 4 through a voltage divider
  * @param  none
  * @retval battery voltage level
  */

uint16_t readBatteryLevel() {
	int analogValue = 0; /*   adc reading for battery is stored in the variable  */
	float batteryVoltage = 0;
	uint16_t batteryLevel = 0; /*    battery voltage   */

	/* enable battery voltage reading */
	enable(BATT_POWER);
	HAL_Delay(10);

	/* Read battery voltage reading */
	analogValue = ADC_ReadChannels(BATTERY_CHANNEL);

	/* disable battery voltage reading */
	disable(BATT_POWER);

	/*battery voltage = ADC value*Vref*2/4096   --12 bit ADC with voltage divider factor of 2 */
	batteryVoltage = (analogValue * 3.3 * ((R1 + R2) / R2)) / 4096;

	/*multiplication factor of 100 to convert to int from float*/
	batteryLevel = (uint16_t) (batteryVoltage * 100);

	return batteryLevel;
}

/**
 *
 * @brief manual control of gpio inorder to enable the power to the sensor
 * @param the gpio to be enabled
 * @retval none
 *
 **/
void enable(uint8_t pin) {

	switch (pin) {
		case 1:
			HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_RESET); //for battery
			break;
		case 2:
			HAL_GPIO_WritePin(TF_Luna_ENABLE_PORT, TF_Luna_ENABLE_PIN, GPIO_PIN_SET);
			break;
}
}

/**
 * @brief manual control of gpio inorder to disable the power to the sensor
 * @param the gpio to be disabled
 * @retval none
 *
 **/
void disable(uint8_t pin) {

	switch (pin) {
		case 1:
			HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_SET); //for battery
			break;
		case 2:
			HAL_GPIO_WritePin(TF_Luna_ENABLE_PORT, TF_Luna_ENABLE_PIN, GPIO_PIN_RESET);

}
}

/**
 * @brief Initialize Distance measurement
 * @param none
 * @retval none
 *
 **/
void distance_measurement_init()
{
	BSP_I2C1_Init();

	GPIO_InitTypeDef initStruct = { 0 };

	initStruct.Pull = GPIO_NOPULL;
	initStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	initStruct.Mode = GPIO_MODE_OUTPUT_PP;

	initStruct.Pin = BATT_ENABLE_PIN;
	HAL_GPIO_Init(BATT_ENABLE_PORT, &initStruct);

	initStruct.Pin = TF_Luna_ENABLE_PIN;
	HAL_GPIO_Init(TF_Luna_ENABLE_PORT, &initStruct);

	enable(TF_LUNA_POWER);
	HAL_Delay(5000);

	TF_Luna_init(&TF_Luna, &hi2c1, TF_Luna_Default_I2C_Address);
	Set_Frame_Rate(&TF_Luna,&TF_Luna_Default_Frame_Rate);

}

/**
 *
 * @brief Read Sensor Data
 * @param structure to store the data
 * @retval none
 *
 **/
void distance_read(lidarData_t *sensor_data)
{
	sensor_data->battery_level = readBatteryLevel();

	enable(TF_LUNA_POWER);
	HAL_Delay(5000);


	Enable_TF_Luna(&TF_Luna);

	getData(&TF_Luna, &sensor_data->Distance, &sensor_data->Signal_Quality, &sensor_data->Temperature);
	sensor_data->sensor_status = printStatus();

	Disable_TF_Luna(&TF_Luna);

	disable(TF_LUNA_POWER);



	APP_PRINTF("Distance = %d\n\r",sensor_data->Distance);
	APP_PRINTF("Signal Quality = %d\n\r",sensor_data->Signal_Quality);
	APP_PRINTF("Battery_Level = %d\n\r",sensor_data->battery_level);

}


